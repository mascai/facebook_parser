###  FB parser

### 1. Installation

Build and install python package
 

### 2. Run on the server
```
nohup python3.6 facebook  &> nohup_facebook_log.out &
```

### 3. Usage
Perform request (e.g. http://0.0.0.0:8086/user?id=4)

### 4 Example

Program returns data

```
{
    "fbid": "4",
    "firstName": "Mark",
    "secondName": "Zuckerberg",
    "birthDate": "14 May 1984",
    "name": "Mark Zuckerberg",
    "sex": "Male",
    "relation": "married",
    "works": [
        {
            "company": "Chan Zuckerberg Initiative",
            "country_id": "",
            "city_id": "",
            "from": "2015",
            "until": "Present",
            "country": "",
            "city": ""
        },
        {
            "company": "Facebook",
            "country_id": "",
            "city_id": "",
            "from": "2004",
            "until": "Present",
            "country": "California",
            "city": "Palo Alto"
        }
    ],
    "educations": [
        {
            "name": "Harvard University",
            "description": "Computer Science and Psychology",
            "from": "2002",
            "graduation": "2004"
        },
        {
            "name": "Phillips Exeter Academy",
            "description": "Classics",
            "from": "",
            "graduation": ""
        },
        {
            "name": "Ardsley High School",
            "description": "High School",
            "from": "1998",
            "graduation": "2000"
        }
    ],
    "currentCity": "Palo Alto, California",
    "homeCity": "Dobbs Ferry, New York",
    "relationPartner": {
        "fbid": "140",
        "firstName": "Priscilla",
        "secondName": "Chan",
        "birthDate": "",
        "name": "Priscilla Chan",
        "sex": "Female",
        "relation": "married",
        "works": [
            {
                "company": "Chan Zuckerberg Initiative",
                "country_id": "",
                "city_id": "",
                "from": "2015",
                "until": "Present",
                "country": "",
                "city": ""
            },
            {
                "company": "UCSF Benioff Children's Hospitals",
                "country_id": "",
                "city_id": "",
                "from": "2012",
                "until": "2015",
                "country": "California",
                "city": "San Francisco"
            },
            {
                "company": "The Harker School",
                "country_id": "",
                "city_id": "",
                "from": "2007",
                "until": "2008",
                "country": "",
                "city": "hanging out with the little ones and trying to explain how the world works."
            },
            {
                "company": "The Harker School",
                "country_id": "",
                "city_id": "",
                "from": "2007",
                "until": "2018",
                "country": "",
                "city": ""
            },
            {
                "company": "FASE",
                "country_id": "",
                "city_id": "",
                "from": "2003",
                "until": "2007",
                "country": "",
                "city": "These little ones inspired me."
            }
        ],
        "educations": [
            {
                "name": "UCSF",
                "description": "University (postgraduate)",
                "from": "2008",
                "graduation": "2012"
            },
            {
                "name": "University of California, San Francisco",
                "description": "M.D.",
                "from": "",
                "graduation": ""
            },
            {
                "name": "Harvard University",
                "description": "Biology",
                "from": "",
                "graduation": ""
            },
            {
                "name": "Harvard University",
                "description": "University",
                "from": "2003",
                "graduation": "Present"
            },
            {
                "name": "Quincy High School",
                "description": "High School",
                "from": "",
                "graduation": ""
            }
        ],
        "currentCity": "Palo Alto, California",
        "homeCity": "Braintree, Massachusetts",
        "degreeOfRelation": "partner",
        "url": "https://mbasic.facebook.com/priscilla?refid=17"
    },
    "relatives": []
}
```
