""" Test facebook/parser """

import os
import json

from unittest import TestCase
from selenium import webdriver

from facebook.parser.utils import parse_time, split_location
from facebook.parser.run_parser import get_facebook_data
from facebook.parser.search import search_users, get_organization_id, get_city_id
from facebook.parser.src import SEX_XPATH, BIRTHDAY_XPATH
from facebook.parser.src import CURR_CITY_XPATH, HOME_CITY_XPATH, NAME_XPATH
from facebook.parser.utils import get_educations, get_works, get_family_status
from facebook.parser.utils import get_relatives, get_data_by_xpath, get_friend_ids
from .common import init_parser


CUR_PATH = os.path.dirname(__file__)

SEARCH_MARK = {
    "name" : "Mark Zuckerberg",
    "location" : "Palo Alto California",
    "university" : "Harvard University",
    "work" : "",
}

SEARCH_ALEX = {
    "name" : "Alex Pepo",
    "location" : "",
    "university" : "",
    "work" : "Activision",
}

SEARCH_IVAN = {
    "name" : "Иван Иванов",
    "location" : "",
    "university" : "",
    "work" : "",
}


class TestParserUtils(TestCase):
    """TestParserUtils"""
    def test_parse_time(self):
        """ Test parse_time() function"""
        self.assertEqual(
            parse_time("March 2016 - February 2017"),
            ('2016', '2017')
        )
        self.assertEqual(
            parse_time("11 September 2018 - Present"),
            ('2018', 'Present')
        )
        self.assertEqual(
            parse_time("2012 - February 2016"),
            ('2012', '2016')
        )

    def test_split_location(self):
        """ Test split_location()"""
        self.assertEqual(
            split_location("Moscow, Russia"),
            ('Moscow', 'Russia')
        )
        self.assertEqual(
            split_location("Russia"),
            ('Russia', '')
        )

class TestParserHtmlFile(TestCase):
    """Test html parsing without login to facebook.com"""
    maxDiff = None
    def __init__(self, *args, **kwargs):
        super(TestParserHtmlFile, self).__init__(*args, **kwargs)
        file_path = CUR_PATH + "/data/Mark.html"
        with open(file_path) as html_file:
            chrome_options = webdriver.ChromeOptions()
            prefs = {"profile.default_content_setting_values.notifications": 2}
            chrome_options.add_experimental_option("prefs", prefs)
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-dev-shm-usage')

            self.driver = webdriver.Chrome(options=chrome_options)
            html_content = html_file.read()
            self.driver.execute_script("document.write('{}')".format(json.dumps(html_content)))

    def test_get_educations(self):
        """ Test create education and EDUCATION_XPATH"""
        expected = [
            {
                'description': 'Computer Science and Psychology',
                'from': '2002',
                'graduation': '2004',
                'name': 'Harvard University'
            },
            {
                'description': 'Classics',
                'from': '',
                'graduation': '',
                'name': 'Phillips Exeter Academy'
            },
            {
                'description': 'High School',
                'from': '1998',
                'graduation': '2000',
                'name': 'Ardsley High School'
            }
        ]
        res = get_educations(self.driver)
        self.assertEqual(res, expected)

    def test_get_works(self):
        """ Test create work and WORK_XPATH"""
        expected = [
            {
                'city': '',
                'city_id': '',
                'company': 'Chan Zuckerberg Initiative',
                'country': '',
                'country_id': '',
                'from': '2015',
                'until': 'Present'},
            {
                'city': 'Palo Alto',
                'city_id': '',
                'company': 'Facebook',
                'country': 'California',
                'country_id': '',
                'from': '2004',
                'until': 'Present'
            }
        ]
        res = get_works(self.driver)
        self.assertEqual(res, expected)

    def test_sex_parsing(self):
        """ Test SEX_XPATH and get_data_by_xpath"""
        expected = "Male"
        self.assertEqual(get_data_by_xpath(self.driver, SEX_XPATH), expected)

    def test_birthday_parsing(self):
        """ Test BIRTHDAY_XPATH"""
        expected = "14 May 1984"
        self.assertEqual(get_data_by_xpath(self.driver, BIRTHDAY_XPATH), expected)

    def test_city_parsing(self):
        """ Test CURR_CITY_XPATH, HOME_CITY_XPATH"""
        expected_curr_city = "Palo Alto, California"
        expected_home_city = "Dobbs Ferry, New York"
        self.assertEqual(get_data_by_xpath(self.driver, CURR_CITY_XPATH), expected_curr_city)
        self.assertEqual(get_data_by_xpath(self.driver, HOME_CITY_XPATH), expected_home_city)

    def test_name_parsing(self):
        """ Test NAME_XPATH"""
        expected = "Mark Zuckerberg"
        self.assertEqual(expected, self.driver.find_element_by_xpath(NAME_XPATH).text)

    def test_family_status(self):
        """ Test get_family_status()"""
        expected = 'married'
        res = get_family_status(self.driver)
        self.assertEqual(expected, res[0])

    def test_relatives(self):
        """ Test get_relatives()"""
        file_path = CUR_PATH + "/data/Priscilla.html"
        with open(file_path) as html_file:
            expected = [['Cousin', 'Louis Ng', ''], ['Mother-in-law', 'Karen Zuckerberg', '']]
            html_content = html_file.read()
            self.driver.execute_script("document.write('{}')".format(json.dumps(html_content)))

            res = get_relatives(self.driver)
            self.assertEqual(expected, res)


class TestParserSource(TestCase):
    """TestParserSource"""
    maxDiff = None
    def __init__(self, *args, **kwargs):
        super(TestParserSource, self).__init__(*args, **kwargs)
        self.fb_parser = init_parser()

    def test_get_facebook_data(self):
        """ Test get_facebook_data()"""

        file_path = CUR_PATH + "/data/Mark.json"
        res_dict = get_facebook_data(self.fb_parser, "4")
        with open(file_path) as file_json:
            expected = json.load(file_json)
            self.assertEqual(res_dict, expected)

    def test_organization_id(self):
        """ Test get_organization_id()"""

        msu_id = get_organization_id(
            self.fb_parser.driver,
            "Московский государственный университет имени М.В.Ломоносова"
        )
        self.assertEqual(msu_id, "424791787567328")

        spbgu_id = get_organization_id(
            self.fb_parser.driver,
            "СПбГУ (Санкт-Петербургский Государственный Университет)"
        )
        self.assertEqual(spbgu_id, "609216069204777")

        oxford_id = get_organization_id(
            self.fb_parser.driver,
            "University of Oxford"
        )
        self.assertEqual(oxford_id, "16686610106")

        google_id = get_organization_id(
            self.fb_parser.driver,
            "Google"
        )
        self.assertEqual(google_id, "104958162837")

        gazprom_id = get_organization_id(
            self.fb_parser.driver,
            "Газпром"
        )
        self.assertEqual(gazprom_id, "243195715748371")

    def test_city_id(self):
        """ Test get_city_id"""
        palo_alto_id = get_city_id(
            self.fb_parser.driver,
            "Palo Alto California"
        )
        self.assertEqual(palo_alto_id, "104022926303756")

        moscow_id = get_city_id(
            self.fb_parser.driver,
            "Moscow Russia"
        )
        self.assertIn(moscow_id, ["115085015172389", "275180320620"])

        spb_id = get_city_id(
            self.fb_parser.driver,
            "Saint Petersburg Russia"
        )
        self.assertEqual(spb_id, "108131085886116")

        omsk_id = get_city_id(
            self.fb_parser.driver,
            "Omsk Russia"
        )
        self.assertEqual(omsk_id, "106041892769481")

        kazan_id = get_city_id(
            self.fb_parser.driver,
            "kazan russia"
        )
        self.assertEqual(kazan_id, "106456079391157")

    def test_search_vip(self):
        """ Test search vip user. Integration test."""
        user_ids = search_users(
            self.fb_parser.driver,
            SEARCH_MARK
        )
        self.assertIn("4", user_ids)

    def test_search(self):
        """ Test search user. Integration test."""

        user_ids = search_users(
            self.fb_parser.driver,
            SEARCH_ALEX
        )
        self.assertIn("100000017196606", user_ids) # found expected user_id

        # search only by name
        user_ids = search_users(
            self.fb_parser.driver,
            SEARCH_IVAN
        )
        self.assertGreater(len(user_ids), 0) # found users

    def test_get_friends(self):
        """Test get_friend_page_ids"""
        expected = ['100041024899593', '100046738601294', '100044413291478']
        fake_user_id = "100055525852354"
        res = get_friend_ids(self.fb_parser.driver, fake_user_id)
        self.assertEqual(expected, res)
