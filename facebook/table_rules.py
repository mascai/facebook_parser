"""Table rules"""

from chtables.rules import value_from
from chtables.extractor_table import TableExtractor
from chtables import CHTable, Column


PERSON_PROTECTED = TableExtractor("person", {
    "fbid": value_from("fbid"),
    "city": value_from("city", default="")
})


PERSON_PRIVATE = CHTable("", "persons", {
    "name": Column("name"),
    "city": Column("city")
}, key="fbid")
