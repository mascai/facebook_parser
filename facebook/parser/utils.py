""" Facebook parser helper functions"""

import re
from logging import getLogger

LOGGER = getLogger("ParserUtils")


def get_data_by_xpath(driver, xpath_pattern):
    """get data by xpath pattern or empty string"""
    res = driver.find_elements_by_xpath(xpath_pattern)
    if res:
        return res[0].text
    return ""

def split_location(location):
    """
        Return pair of strings (city, country)
        Found a few location examples
            1. Moscow, Russia
            2. Russia
    """
    res_city = ""
    res_country = ""

    vec = location.split(',')
    if len(vec) == 1:
        res_city = vec[0]
    elif len(vec) == 2:
        res_city, res_country = vec[0], vec[1]
    return res_city.strip(), res_country.strip()


def is_time(period):
    """
        Check if string is time period

        Found a few types of periods
            1. March 2016 - February 2017
            2. 11 September 2018 - Present
            3. 2012 - February 2016
            4. 11 September 2018 - Present

        Each period contains " - " and 4-digits number
    """

    contain_minus = False
    contain_num = False

    if period.find(" - ") != -1:
        contain_minus = True

    if re.search(r"\d\d\d\d", period) is not None:
        contain_num = True

    return contain_minus and contain_num


def parse_time(period):
    """ Return (start, end) from time period"""
    res_start = ""
    res_end = ""

    vec = period.split(" - ")
    if len(vec) != 2:
        LOGGER.info("Invalid time period string period= %s", period)
        return "", ""

    # parse start
    start = vec[0]
    search_res = re.search(r"\d\d\d\d", start)
    if search_res is not None:
        res_start = search_res.group(0)

    # parse end
    end = vec[1]
    if end == 'Present':
        res_end = end
    else:
        search_res = re.search(r"\d\d\d\d", end)
        if search_res is not None:
            res_end = search_res.group(0)

    return res_start, res_end

def get_educations(driver):
    """
    Gel list of dicts with education related data
    Exapmles of raw_data
    ['Harvard University', 'Computer Science and Psychology',
    '30 August 2002 - 30 April 2004']
    ['Phillips Exeter Academy', 'Classics', 'School year 2002']
    ['Ardsley High School', 'High School', 'September 1998 - June 2000']
    """
    res = [] # list of education dicts
    raw_educations = driver.find_elements_by_xpath("//div[@id='education']/div/div/div")
    if raw_educations:
        educations = [cur.text.split("\n") for cur in raw_educations]
        LOGGER.debug("Found %s education items", len(educations))
        for edu in educations:
            cur_edu = create_education(edu)
            if cur_edu:
                LOGGER.debug("Append education = %s", cur_edu)
                res.append(cur_edu)

    return res

def create_education(edu):
    """
    Convert edu=["University name", "Description", "Time period"] to dict
    """
    res = {
        "name": "",
        "description": "",
        "from": "",
        "graduation": ""
    }
    size = len(edu)
    if size == 0:
        return None # empty list
    name = edu[0]
    description = ""
    start_time = ""
    end_time = ""
    if size > 1:
        if is_time(edu[1]):
            start_time, end_time = parse_time(edu[1])
        else:
            description = edu[1] # Time_period text goes after Description
            if size > 2 and is_time(edu[2]):
                start_time, end_time = parse_time(edu[2])
    res["name"] = name
    res["description"] = description
    res["from"] = start_time
    res["graduation"] = end_time
    return res

def get_works(driver):
    """ Gel list of Work dicts"""
    res = [] # list of Work dicts

    raw_works = driver.find_elements_by_xpath("//div[@id='work']/div/div/div")
    if raw_works:
        works = [cur.text.split("\n") for cur in raw_works]
        for work in works:
            cur_work = create_work(work)
            if cur_work:
                res.append(cur_work)
    return res


def create_work(work):
    """
    Convert raw list [CompanyName, work[1], PeriodOfWork, City, Comment] to dict
    Disabled "Method could be a function"
    because method logically relates to class
    """
    res = {
        "company": "",
        "country_id": "",
        "city_id": "",
        "from": "",
        "until": "",
        "country": "",
        "city": ""
    }
    size = len(work)
    if size == 0:
        return None # empty list
    name = work[0]
    country = ""
    city = ""
    start_time = ""
    end_time = ""

    if size > 1:
        if is_time(work[1]):
            start_time, end_time = parse_time(work[1])
            if size > 2:
                # We know that Company location goes after time pariod
                city, country = split_location(work[2])
        elif size > 2 and is_time(work[2]):
            start_time, end_time = parse_time(work[2])
            if size > 3:
                # We know that Company location goes after time pariod
                city, country = split_location(work[3])
    res["company"] = name
    res["from"] = start_time
    res["until"] = end_time
    res["city"] = city
    res["country"] = country
    return res

def get_family_status(driver):
    """ Return
            ("single", "") - in case of single or no information
            ("married", "partner_url") - in case of married and url of partner in case of exist

        Examples of field text:
            1) Married to Priscilla Chan since 19 May 2012
            2) Married
    """
    is_married = "single"
    partner_url = ""

    raw_status = driver.find_elements_by_xpath(
        "//div[@id='relationship']/div/div/div/div/div")
    if raw_status:
        raw_text = raw_status[0].text
        if raw_text.find("Married") != -1:
            is_married = "married"
        # Try to extract partner url
        raw_href = driver.find_elements_by_xpath(
            "//div[@id='relationship']/div/div/div/div/div/a")
        if raw_href:
            partner_url = raw_href[0].get_attribute('href')

    return is_married, partner_url

def get_relatives(driver):
    """ Return list of family members e.g. [[Mother, name, url], [father, name, url]]"""

    res = []
    persons = driver.find_elements_by_xpath(
        "//div[@id='family']/div/div/div/header")
    if persons:
        cur_id = 0
        for person in persons:
            person_url = ""

            name, mode = person.text.split('\n')
            raw_hrefs = driver.find_elements_by_xpath(
                "//div[@id='family']/div/div/div/header/h3/a")
            if raw_hrefs and len(raw_hrefs) > cur_id:
                person_url = raw_hrefs[cur_id].get_attribute('href')
            res.append([mode, name, person_url])
            cur_id += 1
    return res

def get_friend_page_ids(driver):
    """ Get ids from current page"""
    res = []
    friends_raw = driver.find_elements_by_xpath("//span[text()='Add Friend']/parent::a")
    LOGGER.info("Found %s friends on the page", len(friends_raw))
    for friend in friends_raw:
        href = friend.get_attribute('href')
        if href:
            cur_id = re.findall(r"add_friend\.php\?id=(\d+)", href)
            if cur_id:
                res.append(cur_id[0])
            else:
                LOGGER.info("Can't find id in href=%s", href)
        else:
            LOGGER.info("Can't find href in friend html")
    return res


def get_friend_ids(driver, user_id):
    """ Get list of friend ids"""
    initial_url = driver.current_url
    driver.get("https://mbasic.facebook.com/profile.php?id={}&v=friends".format(user_id))
    res = []
    next_link = driver.find_elements_by_xpath("//span[text()='See more friends']")
    iteration = 1
    while iteration <= 10:
        if iteration != 1:
            next_link[0].click()
        res.extend(get_friend_page_ids(driver))
        iteration += 1
        next_link = driver.find_elements_by_xpath("//span[text()='See more friends']")
        if not next_link:
            break

    driver.get(initial_url) # go to initial page
    return res
