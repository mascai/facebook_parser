""" Set of functions to run Selenium parser"""

from logging import getLogger
from facebook.parser.src import parse_base
from facebook.parser.utils import get_relatives, get_family_status

LOGGER = getLogger("RunParser")


def parse_relative(parser, url, degree_of_relation):
    """
    Parse ralative of the main person
    Return dict with info about person
    """

    current_url = parser.driver.current_url
    res = parse_base(parser, url) # go to the relative page and parse
    parser.driver.get(current_url) # go to initial url
    res["degreeOfRelation"] = degree_of_relation
    res["url"] = url
    return res


def get_facebook_data(parser, url_or_id):
    """
        Full parsing of one person
        parser - FacebookRequestManager object
        url_or_id - person page url or facebook_user_id

        Return dict about person
    """
    all_relatives = []
    res = parse_base(parser, url_or_id)

    _, url = get_family_status(parser.driver)
    res["relationPartner"] = ""
    if res["relation"] == "married" and url != "":
        res["relationPartner"] = parse_relative(parser, degree_of_relation="partner", url=url)

    relatives = get_relatives(parser.driver)
    for cur in relatives:
        if url != "":
            all_relatives.append(parse_relative(parser, degree_of_relation=cur[0], url=cur[2]))
        else:
            LOGGER.info("No url for %s", cur[0])
    res["relatives"] = all_relatives
    return res
