""" Search facebook users"""

import re
import json
import base64


PATTERN_ID = re.compile(r'id=(\d+)')


def convert_to_base64(data):
    """ Convert utf-8 string to base64 string"""
    encoded_bytes = base64.b64encode(data.encode("utf-8"))
    encoded_str = str(encoded_bytes.rstrip(b"="), "utf-8")
    return encoded_str


def get_organization_id(driver, name):
    """ Get organization id by name """
    organization_id = ""
    driver.get("https://mbasic.facebook.com/search/pages/?q="+ name + "&epa=SEARCH_BOX")
    res = driver.find_elements_by_xpath("//a[contains(@aria-label,'Like Page')]")
    if res:
        container = res[0] # chose the first match
        href = container.get_attribute('href')
        if href:
            search_res = PATTERN_ID.findall(href) # extract id from url
            if search_res:
                organization_id = search_res[0]
    return organization_id


def get_city_id(driver, city_name):
    """ Get city_id by city_name in format '<city> <country>'"""
    city_id = ""

    driver.get("https://mbasic.facebook.com/search/places/?q=" + city_name)
    containers = driver.find_elements_by_xpath("//span[contains(text(), 'City')]")
    for container in containers:
        city_data = container.find_elements_by_xpath(".//following::td/a")
        if city_data:
            href = city_data[0].get_attribute("href")
            if href:
                search_res = PATTERN_ID.findall(href)
                if search_res:
                    city_id = search_res[0]
                    if city_id:
                        break
    return city_id


def make_url_filter(driver, filters):
    """ Make url view of the filter"""
    res = {}
    if filters["work"]:
        work_id = get_organization_id(driver, filters["work"])
        if work_id:
            arguments = {}
            arguments["name"] = "users_employer"
            arguments["args"] = work_id
            res["employer"] = arguments

    if filters["university"]:
        edu_id = get_organization_id(driver, filters["university"])
        if edu_id:
            arguments = {}
            arguments["name"] = "users_school"
            arguments["args"] = edu_id
            res["school"] = arguments

    if filters["location"]:
        city_id = get_city_id(driver, filters["location"])
        if city_id:
            arguments = {}
            arguments["name"] = "users_location"
            arguments["args"] = city_id
            res["school"] = arguments

    encoded_str = convert_to_base64(json.dumps(res))

    url = "https://mbasic.facebook.com/search/people/?q=" \
        + filters["name"] + "&epa=FILTERS&filters=" \
        + encoded_str + "%3D%3D"
    return url

def search_users(driver, filters):
    """ Get list of user ids by filters"""
    res = []
    url = make_url_filter(driver, filters)
    if url:
        driver.get(url)
        users = driver.find_elements_by_xpath(
            "//a[contains(@href,'/a/mobile/friends/add_friend.php')]")
        vip_users = driver.find_elements_by_xpath(
            "//a[contains(@href,'/a/subscribe.php')]")
        users.extend(vip_users)
        for user in users:
            href = user.get_attribute("href")
            if href:
                search_res = PATTERN_ID.findall(href) # extract id from url
                if search_res:
                    res.append(search_res[0])
    return res
