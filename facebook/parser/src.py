""" User defined types"""

import json
import time
import re

from logging import getLogger
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.common.exceptions import InvalidSessionIdException
from pyvirtualdisplay import Display

from facebook.parser.utils import  get_data_by_xpath, get_works, get_educations, get_family_status

LOGGER = getLogger("FacebookApi")


BIRTHDAY_XPATH = "//span[text()='Date of birth']//following::td//div"
SEX_XPATH = "//span[text()='Gender']//following::td//div"
CURR_CITY_XPATH = "//span[text()='Current town/city']//following::td//a"
HOME_CITY_XPATH = "//span[text()='Home town']//following::td//a"
WORK_XPATH = "//div[@id='work']/div/div/div"
NAME_XPATH = "//span/div/span/strong"

BASIC_URL = 'https://mbasic.facebook.com/' # mobile version of facebook

class FacebookRequestManager:
    """ Selenium-based parser"""
    def __init__(self, config_data):
        LOGGER.info("Create FacebookRequestManager object")

        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        self.cur_login = config_data["login"]
        self.cur_password = config_data["password"]
        self.config_data = config_data
        self.login(self.cur_login, self.cur_password)

    def __del__(self):
        """ Destructors are called when an object gets destroyed"""
        try:
            self.driver.quit()
            self.display.stop()
            LOGGER.info("Selenium driver closed")
        except InvalidSessionIdException:
            LOGGER.info("Selenium driver already closed")

    def login(self, login, password):
        """ Login to facebook.com"""
        LOGGER.info("Start login for user=%s", login)
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        if self.config_data["proxy"]:
            LOGGER.info("Set proxy %s", self.config_data["proxy"])
            chrome_options.add_argument('--proxy-server=%s' % self.config_data["proxy"])

        self.driver = webdriver.Chrome(
            options=chrome_options,
            executable_path=self.config_data["chromedriver_path"]
        )
        self.wait = WebDriverWait(self.driver, 10)
        self.driver.get(BASIC_URL)
        self.driver.find_element_by_name('email').send_keys(login)
        self.driver.find_element_by_name('pass').send_keys(password)
        self.driver.find_element_by_name('login').click()
        LOGGER.info("Finish login for user=%s", login)

    def change_account(self):
        """ Change facebook account"""
        cur_data = {
            "login": self.cur_login,
            "password": self.cur_password
        }
        LOGGER.info("Start change_account. Current user=%s", self.cur_login)
        users = json.loads(self.config_data["users"]) # dict vith users login and password
        if cur_data in users:
            pos = users.index(cur_data)
            self.cur_login = users[(pos + 1) % len(users)]["login"] # get next login
            self.cur_password = users[(pos + 1) % len(users)]["password"]
            self.driver.close()
            self.login(self.cur_login, self.cur_password)
        else:
            LOGGER.error("Can't find login=%s, passw=%s", self.cur_login, self.cur_password)
        LOGGER.info("Finish change_account. Current user=%s", self.cur_login)

    def get_url_safe(self, url):
        """get_url_safe"""
        while True:
            try:
                self.driver.get(url)
                break
            except TimeoutException as cur_exception:
                LOGGER.error("Handled exception %s", cur_exception)
                LOGGER.error("Sleep 5 seconds and try again")
                time.sleep(5)

    def get_user_by_url(self, url):
        """get_user_by_url"""
        LOGGER.debug("parse url= %s", url)
        self.driver.get(url)

    def get_user_by_id(self, user_id):
        """get_user_by_id"""
        url = BASIC_URL + 'profile.php?id=' + str(user_id)
        self.get_user_by_url(url)

    def find_element_by_xpath_safe(self, path):
        """find_element_by_xpath_safe"""
        try:
            return self.driver.find_element_by_xpath(path)
        except (NoSuchElementException, TimeoutException) as cur_exception:
            LOGGER.error("Handled exception %s", cur_exception)
            return None

    def find_elements_by_xpath_safe(self, path):
        """find_elements_by_xpath_safe"""
        try:
            return self.driver.find_elements_by_xpath(path)
        except (NoSuchElementException, TimeoutException) as cur_exception:
            LOGGER.error("Handled exception %s", cur_exception)
            return None

    def get_user_id(self):
        """Return user id or empty string"""
        res = ""
        user_id_xpath = "//div[@id='profile_cover_photo_container']/a"
        raw_id = self.find_element_by_xpath_safe(user_id_xpath)

        # Example of url
        # photo.php?fbid=10103832396388711&id=4&set=a.941146602501&source=44
        # we need &id=4
        if raw_id:
            url = raw_id.get_attribute('href')
            if url:
                pos = re.findall(r"&id=(\d+)", url)
                if pos and pos[0].isdigit():
                    res = pos[0]
                else:
                    LOGGER.info("Can't find user_id in url=%s", url)
            else:
                LOGGER.info("Can't find href attribute")
        else:
            LOGGER.info("Can't find xpath=%s", user_id_xpath)
        return res

    def get_first_name(self):
        """get_first_name"""
        res = self.find_element_by_xpath_safe(NAME_XPATH)
        if res:
            vec = res.text.split()
            if len(vec) > 0:
                return vec[0]
            LOGGER.info("Can't split %s", res.text)
        return ""

    def get_second_name(self):
        """get_second_name"""
        res = self.find_element_by_xpath_safe(NAME_XPATH)
        if res:
            vec = res.text.split(maxsplit=1)
            if len(vec) > 1:
                return vec[1]
            LOGGER.info("Can't split %s", res.text)
        return ""


def parse_base(parser, url_or_id):
    """
        Parsing of the common fields
        Return dict about person
    """
    res = {}

    if url_or_id.isdigit():
        parser.get_user_by_id(url_or_id)
        res["fbid"] = url_or_id
    else:
        parser.get_user_by_url(url_or_id)
        res["fbid"] = parser.get_user_id()

    res["firstName"] = parser.get_first_name()
    res["secondName"] = parser.get_second_name()
    res["birthDate"] = get_data_by_xpath(parser.driver, BIRTHDAY_XPATH)
    res["name"] = '{} {}'.format(res["firstName"], res["secondName"])
    res["sex"] = get_data_by_xpath(parser.driver, SEX_XPATH)
    res["relation"], _ = get_family_status(parser.driver)
    res["works"] = get_works(parser.driver)
    res["educations"] = get_educations(parser.driver)
    # don't defined in specification, but may be usefull
    res["currentCity"] = get_data_by_xpath(parser.driver, CURR_CITY_XPATH)
    res["homeCity"] = get_data_by_xpath(parser.driver, HOME_CITY_XPATH)

    return res
