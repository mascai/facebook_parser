"""FacebookService"""

import json
import threading

from typing import Optional, Tuple
from datetime import datetime
from logging import getLogger
from chtables.cache import Cache
from selenium.common.exceptions import InvalidSessionIdException

from facebook.parser.run_parser import get_facebook_data
from facebook.parser.src import FacebookRequestManager


class FacebookService:
    """
        FacebookService combines
        FacebookRequestManager -- extract data from facebook.com with selenium
        Cache -- cache data
        DbProtectedSender -- send data to Clickhouse
    """

    LIMIT = 500
    CACHE_INVALIDATION_DELTA = 300

    def __init__(self, cache: Cache, config_data) -> None:
        self._cache = cache
        self.logger = getLogger('facebook_service')
        self.lock = threading.Lock()
        self.config_data = config_data
        self._parser = self.create_fb_request_manager()

    def create_fb_request_manager(self):
        """ Return FbRequestManager object"""
        self.logger.info("Create FacebookRequestManager")
        return FacebookRequestManager(self.config_data)


    def get_content_from_cache(self, key) -> Optional[Tuple[datetime, dict]]:
        """Return data from cache or None"""
        cache_data = self._cache.get(key)
        if cache_data is None:
            return None

        self.logger.info("Successfully obtain content from Cache by key[%s]", cache_data[1])
        return cache_data[0], cache_data[2]

    def get_content_from_facebook(self, user_id):
        """Obtain data from FacebookRequestManager and update cache"""
        person = self.get_content_safe(user_id)
        content = json.dumps(person, indent=4) # convert to json string

        if self.check_data(person):
            cache_key = person["fbid"]
            self._cache.put(cache_key, content)
            self.logger.info("Successfully saved data into cache with key [%s]", cache_key)
        else:
            self._parser.change_account()
            person = self.get_content_safe(user_id)

        return person

    def get_content_safe(self, user_id):
        """ Safe call of get_facebook_data"""
        with self.lock:
            person = {}
            try:
                person = get_facebook_data(self._parser, user_id) # return Person dict
            except InvalidSessionIdException:
                # Relaunch FacebookRequestManager in case of failed session
                self._parser = FacebookRequestManager(self.config_data)
                person = get_facebook_data(self._parser, user_id)
        return person

    def check_data(self, data):
        """
        Reason of check -- facebook.com blocks parser
        Logic of check
        1. If data with empty "firstName"
        2. Then we have to find text
        "We limit how often you can post, comment or do other things in a \
        given amount of time in order to help protect the community from spam. \
        You can try again later."
        2.2 If we found the text, the change user-account
        2.3 Else do nothing
        """
        if data["firstName"] == "":
            page_html = self._parser.driver.page_source
            if page_html.find("We limit how often you can post, comment") != -1:
                return False
        return True
