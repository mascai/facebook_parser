""" Contains class for starting a service"""

import logging
import json

from typing import Dict, List

from chtables.ch_pool import ChPool
from chtables.cache import Cache
from program_skel import Program
from program_skel.datasource_server import ThreadedHTTPServer, HTTPError
from facebook.services.facebook_service import FacebookService


LOGGER = logging.getLogger("FacebookApi")


def get_request_handler(service: FacebookService) -> callable:
    """ Handler for FacebookApi"""
    def user_handler(params: Dict[str, List[str]], _):
        """
        Handle requests e.g. <host>/user?id=4
        params = {'id': ['4']}
        """

        LOGGER.debug(
            "Params = %s", params)
        if 'id' in params and len(params["id"]) > 0 and params["id"][0].isdigit():
            user_id = params["id"][0]

            answer = service.get_content_from_cache(user_id)
            if answer:
                return answer[1], answer[0], 0
            LOGGER.info("Did not find %s in Data Base", user_id)
            answer = service.get_content_from_facebook(user_id)

            return json.dumps(answer, indent=4), None, 1

        raise HTTPError(400, "Request must be /user?id=<number>")

    return user_handler


class FacebookApi(Program):
    """The main program class"""

    def defaultconfig(self):
        """Sets default config parameters"""

        self.addconfig("login", "alekmosk25@gmail.com", "facebook page login")
        self.addconfig("password", "Javac@v@4", "facebook page password")
        self.addconfig("chromedriver_path", "/usr/local/bin/chromedriver")

        login_data = """
[
   {
      "login":"alekmosk25@gmail.com",
      "password":"Javac@v@4"
   },
   {
      "login":"89100037053",
      "password":"Javac@v@3"
   }
]
"""
        self.addconfig("users", login_data, "add users facebook login data")

        self.addconfig("bind_address", "0.0.0.0", "Address to listen")
        self.addconfig("bind_port", "8086", "Port to listen")
        self.addconfig("cache_invalidation_delta", "604800", "Cache invalidation delta")
        self.addconfig("proxy", "", 'Proxy server "IP:PORT" or "HOST:PORT"')

        # Clickhouse configs
        self.addconfig("clickhouse_host", "127.0.0.1", "Ip of the clickhouse")
        self.addconfig("clickhouse_port", "9000", "Clickhouse port") # server
        self.addconfig('clickhouse_user', 'default', "Username for clickhouse connection")
        self.addconfig('clickhouse_password', '652007', "Password for clickhouse connection")

        self.addconfig('ch_cases_table', 'cases',
                       "Old way saving is turned on if table name is not empty")
        self.addconfig('clickhouse_db', 'db_facebook',
                       "Destination Clickhouse database name for protected tables")
        self.addconfig('ch_protected_db', 'protected', "Protected DB in clickhouse")

        self.addconfig('etl_service_url', '', "Url to send import request after sending finish")

        self.addconfig('insert_private', 'True', "Insert data to private table [True/False]")
        self.addconfig('insert_protected', 'False', "Insert data to protected table [True/False]")
        self.addconfig('expiration_interval', '1 SECOND')


    def run(self):
        """The actual program starts here"""
        config_keys = [
            "login", "password", "chromedriver_path", "proxy", "users"
        ]
        config_data = {}
        for key in config_keys:
            config_data[key] = self.getcfg(key)

        ch_pool = ChPool(
            self.getcfg("clickhouse_host"),
            self.getcfg("clickhouse_port", int),
            user=self.getcfg("clickhouse_user"),
            password=self.getcfg("clickhouse_password")
        )
        cache_instance = Cache(
            ch_pool,
            self.getcfg("clickhouse_db"),
            self.getcfg('expiration_interval')
        )
        cache_instance.make_table()

        address = (self.getcfg('bind_address'), self.getcfg('bind_port', int))
        LOGGER.debug("Create server instance")
        server = ThreadedHTTPServer(address)

        fb_service = FacebookService(
            cache_instance,
            config_data
        )
        FacebookService.CACHE_INVALIDATION_DELTA = self.getcfg("cache_invalidation_delta", int)

        server.register_handler(
            '/user',
            get_request_handler(fb_service),
            method='GET'
        )
        server.serve_forever()
