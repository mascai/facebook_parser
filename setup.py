#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Standart script for the rpm package creation"""
from distutils.core import setup
from os.path import realpath, join
from os import getenv
from sys import path

CURDIR = realpath(path[0])
VERSION = getenv("VERSION", "0.0.0")
RELEASE = getenv("REVISION", "1")

setup(
    name="Facebook",
    version=VERSION,
    description="FacebookRequestManager & http proxy for providing data",
    author="Garda Technologies",
    url="https://gardatech.ru",
    license="Proprietary",
    requires=["GardaSharedPython"],
    packages=['facebook', 'facebook.parser', 'facebook.services'],
    data_files=[
        ("/usr/lib/systemd/system", ["rpm_building/facebook.service"]),
    ],
    options={
        "bdist_rpm": {
            "release": RELEASE,
            "post_install": join(CURDIR, "rpm_building", "post.sh"),
            "pre_uninstall": join(CURDIR, "rpm_building", "preun.sh"),
            "requires": ["GardaSharedPython"]
        }
    }
)
